# Gluon Splitting Tagger
This is a collection of informations for the gluon splitting tagger based on DL1

## Technicalities
The variable ```HadronConeExclExtendedTruthLabelID``` allows to further distinguish the jet categories:

| HadronConeExclExtendedTruthLabelID | Category    |
| ------------- | ---------------- |
| 0            | light jets   |
| 4            | c-jets   | 
| 5            | b-jets   | 
| 15            | tau-jets   | 
| 44            | double c-jets   | 
| 55            | double b-jets   | 




## Strategy

### Ntuple Preparation
The double b-jets will be taken from Znunu samples (maybe also Zmumu) and the other categories from ttbar (Z') events.

After the [derivation](#derivations) are available, the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) is used to produce ntuples for the Sherpa MC samples.
The output will be h5 files which can be further processed using the [hdf5-manipulator](https://gitlab.cern.ch/ATLAS-Herten/hdf5_manipulator). 

First the single MC samples can be merged using ```merge_big.py```. Afterwards the those files can be combined using ```merge_ntuples.py``` where only the bb-categories are written to file.

The ttbar and Z' samples are processed similar to the DL1 workflow.





### Weighting 
In Sherpa the samples are produced in different pt slices. An important step is to figure out how treat those slices -> applying weights?

* One option is to use all the double b-jets and weight them to the pT and |eta| distribution of the ttbar sample.
* What pT range do the Sherpa samples cover?
* Is it useful to use the also Z' ?

### Network structure




## MC samples

### Derivations
The Sherpa samples are extracted from [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AutomaticCentralPage?MYVAR=http://pmg-centralmc15.web.cern.ch/PMG-CentralMC15/NewCentralPage/CentralMC16ProductionPage/MainTwiki_1fbcb7303726cd9b84950bb1ac491520.twiki) and [FTAG1](https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/21422/) derivations were requested. On top of those derivations Znunu derivations were requested [here](https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/22421/).
They can be found in rucio via:
```
rucio ls mc16_13TeV:mc16_13TeV.*.Sherpa_221_*_Z*DAOD_FTAG1.e*_s3126_r9364_r9315_p3703

```
and for the Znunu samples:
```
rucio ls mc16_13TeV:mc16_13TeV.*.Sh_221_*_Znunu_*.DAOD_FTAG1.e*_s3126_r9364_r9315_p3703
```
<!--### Ntuples-->
<!--The ntuples are available on rucio with the names:-->

<!--user.mguth:user.mguth.364209.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364111.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364215.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364199.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364126.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364213.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364203.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364139.btagTraining.e5313_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364105.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364207.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364136.btagTraining.e5307_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364122.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364119.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364130.btagTraining.e5307_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364133.btagTraining.e5307_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364205.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364112.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364108.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364211.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364116.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364201.btagTraining.e5421_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364141.btagTraining.e5307_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364102.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364113.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364127.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364125.btagTraining.e5299_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5 user.mguth:user.mguth.364140.btagTraining.e5307_s3126_r9364_r9315_p3703.EMTopo.2019-01-07-T141142-R22900_output.h5-->



### EMTopo NTuples with new Taggers (PFlow trained RNNIP and SMT)

| Sample | Rucio dataset    |  FTAG1 derivations|
| ------------- | ---------------- | ---------------- |
| MC16a_ttbar | user.mguth.410470.btagTraining.e6337_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-26-T115915-R27163_output.h5 | mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r9364_p3703 |
| MC16a_Zprime | user.mguth.427080.btagTraining.e5362_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-26-T115915-R27163_output.h5 | mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG1.e5362_s3126_r9364_p3703 |
| | | |
| Zmumu |user.mguth:user.mguth.364102.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Zmumu |user.mguth:user.mguth.364113.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Zmumu |user.mguth:user.mguth.364105.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Zmumu |user.mguth:user.mguth.364112.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Zmumu |user.mguth:user.mguth.364111.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Zmumu |user.mguth:user.mguth.364108.btagTraining.e5271_s3126_r9364_r9315_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_FTAG1.e5271_s3126_r9364_r9315_p3703 |
| Znunu |user.mguth:user.mguth.366011.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366011.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366017.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366017.Sh_221_NN30NNLO_Znunu_PTV280_500_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366014.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366014.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366035.btagTraining.e7033_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366035.Sh_221_NN30NNLO_Znunu_PTV280_500_CVetoBVeto.deriv.DAOD_FTAG1.e7033_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366013.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366013.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366012.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366012.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366016.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366016.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366010.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366010.Sh_221_NN30NNLO_Znunu_PTV70_100_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |
| Znunu |user.mguth:user.mguth.366015.btagTraining.e6695_s3126_r9364_p3703.EMTopo_IPRNN.2019-03-25-T143846-R30842_output.h5 |mc16_13TeV:mc16_13TeV.366015.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_BFilter.deriv.DAOD_FTAG1.e6695_s3126_r9364_p3703 |


## Useful Material

[Thesis](http://hal.in2p3.fr/tel-01681700/document) Thomas Calvet